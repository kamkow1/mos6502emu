package mos6502emu

import "core:os"
import "core:fmt"
import "core:log"
import "core:mem"
import "core:math"
import "core:strings"
import "core:strconv"

Opcode :: enum u8 {
    NOP = 0xea,
    JMP = 0x4c,
    LDA_IMMEDIATE_MODE = 0xa9,
    ADC_IMMEDIATE_MODE = 0x69,
}

Registers :: struct {
    pc: u16, // program counter
    a: u8,   // accumulator
    p: u8,   // flag register
    x: u8,   // index register X
    y: u8,   // index register y
}

Flags :: enum u8 {
    FLAG_CARRY = 1,
    FLAG_ZERO  = 2,
    FLAG_INTERRUPT_DISABLE = 4,
    FLAG_DECIMAL  = 8,
    FLAG_BREAK    = 16,
    FLAG_RESERVED = 32,
    FLAG_OVERFLOW = 64,
    FLAG_NEGATIVE = 128,
}

Cpu :: struct {
    regs: Registers,
    memory_ptr: ^u8,
    debug_mode: bool,
}

cpu_increment_pc :: proc(cpu: ^Cpu) {
    cpu.regs.pc += 1
}

// no operation
cpu_nop :: proc(cpu: ^Cpu) { }

// absolute jump to a 16 bit address
cpu_jmp :: proc(cpu: ^Cpu) {
    addr_low := ((cast(^u8)mem.ptr_offset(cpu.memory_ptr, cpu.regs.pc))^)
    cpu_increment_pc(cpu)
    addr_high := ((cast(^u8)mem.ptr_offset(cpu.memory_ptr, cpu.regs.pc))^)
    addr := (cast(u16)addr_low) | (cast(u16)addr_high)<<8
    cpu.regs.pc = addr
}

// load accumulator in immediate mode
cpu_lda_immediate_mode :: proc(cpu: ^Cpu) {
    value := ((cast(^u8)mem.ptr_offset(cpu.memory_ptr, cpu.regs.pc))^)
    cpu.regs.a = value

    if value == 0 {
        cpu.regs.p |= cast(u8)Flags.FLAG_ZERO
    }
    if cpu.regs.a & 0x40 == 1 {
        cpu.regs.p |= cast(u8)Flags.FLAG_NEGATIVE
    }

    cpu_increment_pc(cpu)
}

// add to accumulator with carry
cpu_adc_immediate_mode :: proc(cpu: ^Cpu) {
    value := ((cast(^u8)mem.ptr_offset(cpu.memory_ptr, cpu.regs.pc))^)
    prev_a := cast(u16)cpu.regs.a

    tmp_a := cast(i16)cpu.regs.a
    tmp_value := cast(i16)value
    tmp_a += tmp_value

    if tmp_a == 0 {
        cpu.regs.p |= cast(u8)Flags.FLAG_ZERO
    } else {
        cpu.regs.p &= ~(cast(u8)Flags.FLAG_ZERO)
    }

    if tmp_a & 0x40 == 1 {
        cpu.regs.p |= cast(u8)Flags.FLAG_NEGATIVE
    } else {
        cpu.regs.p &= ~(cast(u8)Flags.FLAG_NEGATIVE)
    }

    if tmp_a > 0xff {
        cpu.regs.p |= cast(u8)Flags.FLAG_CARRY
    } else {
        cpu.regs.p &= ~(cast(u8)Flags.FLAG_CARRY)
    }

    cpu.regs.a = cast(u8)tmp_a

    cpu_increment_pc(cpu)
}

Cpu_Handler :: #type proc(cpu: ^Cpu)

cpu_handler_map : map[Opcode]Cpu_Handler = {
    .NOP = cpu_nop,
    .JMP = cpu_jmp,
    .LDA_IMMEDIATE_MODE = cpu_lda_immediate_mode,
    .ADC_IMMEDIATE_MODE = cpu_adc_immediate_mode,
}

cpu_handle_opcode :: proc(cpu: ^Cpu, opcode: Opcode) -> bool {
    log.infof("Handling opcode %v", opcode)

    handler, ok := cpu_handler_map[opcode]
    if !ok {
        log.errorf("Unimplemented opcode %#2x", cast(u8)opcode)
        return false
    }
    handler(cpu)
    return true
}

Dbg_Cmd_Type :: enum {
    DBG_NEXT,
    DBG_CPU_INFO,
}

Dbg_Cmd_Arg :: union {
    string,
    bool,
    int,
}

Dbg_Cmd :: struct {
    str_name: string,
    type: Dbg_Cmd_Type,
    args: [dynamic]Dbg_Cmd_Arg,
}

dbg_get_command :: proc(s: string) -> (cmd: Dbg_Cmd, ok: bool) {
    ok = false

    dbg_commands := make(map[string]Dbg_Cmd)
    defer delete(dbg_commands)
    dbg_commands["next"] = Dbg_Cmd { str_name = "next", type = .DBG_NEXT }
    dbg_commands["cpu-info"] = Dbg_Cmd { str_name = "cpu-info", type = .DBG_CPU_INFO }

    tokens := strings.split(s, " ")
    defer delete(tokens)

    if len(tokens) == 0 {
        return
    }

    cmd, ok = dbg_commands[tokens[0]]
    if !ok {
        return
    }

    for j in 1..=len(tokens)-1 {
        ok : bool
        int_value : int
        bool_value : bool

        int_value, ok = strconv.parse_int(tokens[j])
        if ok {
            append(&cmd.args, int_value)
        }
        bool_value, ok = strconv.parse_bool(tokens[j])
        if ok {
            append(&cmd.args, bool_value)
        }

        if !ok {
            append(&cmd.args, tokens[j])
        }
    }

    return
}

cpu_execute_program :: proc(cpu: ^Cpu, memory: ^u8, memory_size: u16) -> bool {
    cpu.memory_ptr = memory
    for {
        if cpu.debug_mode {
            buffer : [0xff]byte
            n, err := os.read(os.stdin, buffer[:])
            if err < 0 {
                log.errorf("Cannot read input")
                return false
            }
            str := string(buffer[:n-1])

            dbg_cmd, ok := dbg_get_command(str)
            if !ok {
                log.errorf("Unknown debugger command %v", str)
                continue
            }

            print_sep_line :: proc() {
                for _ in 0..=31 {
                    fmt.print("-")
                }
                fmt.print("\n")
            }

            switch dbg_cmd.type {
                case .DBG_NEXT:
                    steps := len(dbg_cmd.args) == 0 ? 1 : dbg_cmd.args[0].(int)
                    for _ in 0..=steps-1 {
                        opcode := (cast(^u8)mem.ptr_offset(cpu.memory_ptr, cpu.regs.pc))^
                        cpu_increment_pc(cpu)
                        if !cpu_handle_opcode(cpu, cast(Opcode)opcode) {
                            return false
                        }
                    }
                case .DBG_CPU_INFO:
                    print_sep_line()

                    fmt.printf("Program Counter: %d (%x)\n", cpu.regs.pc, cpu.regs.pc)
                    fmt.printf("Accumulator: %d (%x)\n", cpu.regs.a, cpu.regs.a)
                    fmt.printf("Flags: %d (%x)\n", cpu.regs.p, cpu.regs.p)
                    fmt.printf("  carry             = %t\n", cpu.regs.p & cast(u8) Flags.FLAG_CARRY == 1)
                    fmt.printf("  zero              = %t\n", cpu.regs.p & cast(u8) Flags.FLAG_ZERO == 1)
                    fmt.printf("  interrupt disable = %t\n", cpu.regs.p & cast(u8) Flags.FLAG_INTERRUPT_DISABLE == 1)
                    fmt.printf("  decimal           = %t\n", cpu.regs.p & cast(u8) Flags.FLAG_DECIMAL == 1)
                    fmt.printf("  break             = %t\n", cpu.regs.p & cast(u8) Flags.FLAG_BREAK == 1)
                    fmt.printf("  reserved          = %t\n", cpu.regs.p & cast(u8) Flags.FLAG_RESERVED == 1)
                    fmt.printf("  overflow          = %t\n", cpu.regs.p & cast(u8) Flags.FLAG_OVERFLOW == 1)
                    fmt.printf("  negative          = %t\n", cpu.regs.p & cast(u8) Flags.FLAG_NEGATIVE == 1)

                    print_sep_line()
            }
        } else {
            opcode := (cast(^u8)mem.ptr_offset(cpu.memory_ptr, cpu.regs.pc))^
            cpu_increment_pc(cpu)
            if !cpu_handle_opcode(cpu, cast(Opcode)opcode) {
                return false
            }
        }
    }

    return true
}

load_program :: proc(memory: ^u8, memory_size: int, bin: []u8) -> bool {
    if len(bin) > memory_size {
        log.errorf("Program size exceeds 64K")
        return false
    }
    log.infof("Program size: %dB", len(bin))

    for i in 0..=len(bin)-1 {
        ptr := mem.ptr_offset(memory, i)
        ptr^ = bin[i]
    }
    return true
}

main1 :: proc() -> int {
    context.logger = log.create_console_logger()

    opts := init_opts()
    defer deinit_opts(&opts)
    {
        using optarg_opt
        add_arg(&opts, "f", .REQUIRED_ARGUMENT, "file")
        add_arg(&opts, "d", .NO_ARGUMENT, "debug")
    }
    getopt_long(os.args, &opts)
    
    cpu : Cpu

    bin_path : string
    for opt in opts.opts {
        switch opt.name {
            case "f":
                bin_path = opt.val.(string)
            case "d":
                cpu.debug_mode = true
        }
    }

    if bin_path == "" {
        log.error("No file provided!")
        return 2
    }
    bin, read_ok := os.read_entire_file(bin_path)
    if !read_ok {
        log.errorf("Could not read file %s", bin_path)
    }

    MEMORY_SIZE :: 64*1024 - 1

    ptr, alloc_err := mem.alloc(MEMORY_SIZE)
    if alloc_err != mem.Allocator_Error.None {
        log.errorf("Could not allocate enough RAM")
        return 1
    }
    memory := cast(^u8)ptr
    if !load_program(memory, MEMORY_SIZE, bin) {
        log.errorf("Could not load program into memory")
        return 1
    }
    if !cpu_execute_program(&cpu, memory, MEMORY_SIZE) {
        log.errorf("Could not execute the program")
        return 1
    }
    return 0
}

main :: proc () { os.exit(main1()) }

