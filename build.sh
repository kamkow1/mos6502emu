#!/bin/sh

set -xe

odin build .

./cc65/bin/ca65 -o nop.o ./nop.S
./cc65/bin/ld65 -o nop.bin -C link.cfg ./nop.o

